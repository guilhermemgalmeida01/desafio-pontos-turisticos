export class TuristicPoints {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".turistic-form");
        this.submit = document.querySelector(".turistic-form-submit-button");
        this.imageinput = document.querySelector(".turistic-form-image-input");
        this.titleinput = document.querySelector(".turistic-form-title-input");
        this.textarea = document.querySelector(".turistic-form-description-textarea");
        this.div = document.querySelector(".new-point-div-main");
    }

    events() {
        this.form.addEventListener("submit", this.addTuristicLocation.bind(this));
    }

    addTuristicLocation(event) {
        event.preventDefault();

        const titleinput = event.target["title"].value;

        const imageinput = event.target["image"].value;

        const textarea = event.target["description"].value;

        if ((titleinput, imageinput, textarea !== "")) {
            const item = {
                Título: titleinput,
                Imagem: imageinput,
                TextArea: textarea,
            };
            this.list.push(item);
            this.renderListItems();
            this.console.log(list);
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                <li class="turistic-image-location">
                    <span>${item.Título}</span>
                    <span>${item.Imagem}</span>
                    <span>${item.TextArea}</span>
                </li>
            `;
        });

        this.div.innerHTML = itemsStructure;
    }
}
